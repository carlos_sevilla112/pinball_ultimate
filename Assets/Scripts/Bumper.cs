﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {


    public float power = 100.0f;
    public float radius = 1.0f;


    void OnCollisionEnter()
    {

        foreach (Collider col in Physics.OverlapSphere(transform.position,radius))
        {

            if (col.GetComponent<Rigidbody>())
            {
                col.attachedRigidbody.AddExplosionForce(power,transform.position, radius);
            }

        }

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
